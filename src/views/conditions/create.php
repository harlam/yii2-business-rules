<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model harlam\yii2\BusinessRules\models\BusinessConditions */

$this->title = 'Create Business Conditions';
$this->params['breadcrumbs'][] = ['label' => 'Business Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-conditions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
