<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model harlam\yii2\BusinessRules\models\BusinessConditionsRules */

$this->title = 'Update Business Conditions Rules: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Business Conditions Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="business-conditions-rules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
