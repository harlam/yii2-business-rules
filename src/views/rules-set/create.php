<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model harlam\yii2\BusinessRules\models\BusinessConditionsRules */

$this->title = 'Create Business Conditions Rules';
$this->params['breadcrumbs'][] = ['label' => 'Business Conditions Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-conditions-rules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
