<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model harlam\yii2\BusinessRules\models\BusinessRules */

$this->title = 'Create Business Rules';
$this->params['breadcrumbs'][] = ['label' => 'Business Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-rules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
