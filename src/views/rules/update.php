<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model harlam\yii2\BusinessRules\models\BusinessRules */

$this->title = 'Update Business Rules: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Business Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="business-rules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
