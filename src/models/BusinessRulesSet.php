<?php

namespace harlam\yii2\BusinessRules\models;

/**
 * This is the model class for table "business_conditions_rules".
 *
 * @property int $id
 * @property int $condition_id
 * @property int $rule_id
 * @property int $weight
 * @property bool $is_active
 * @property BusinessRules $rule
 * @property BusinessConditions $condition
 */
class BusinessRulesSet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_rules_set';
    }

    public static function getRulesSet(string $group): \harlam\BusinessRules\BusinessRulesSet
    {
        $result = new \harlam\BusinessRules\BusinessRulesSet();
        $arRules = self::find()
            ->with('condition', 'rule')
            ->where(['group' => $group, 'is_active' => true])
            ->orderBy(['weight' => SORT_DESC])->all();
        /** @var BusinessRulesSet $arRule */
        foreach ($arRules as $arRule) {
            $result->addRule($arRule->condition->getBusinessCondition(), $arRule->rule->getBusinessRule());
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'condition_id', 'rule_id'], 'required'],
            [['condition_id', 'rule_id', 'weight'], 'default', 'value' => null],
            [['condition_id', 'rule_id', 'weight'], 'integer'],
            [['group'], 'string', 'max' => 255],
            [['is_active'], 'boolean'],
            [['condition_id', 'rule_id'], 'unique', 'targetAttribute' => ['condition_id', 'rule_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group' => 'Object Type',
            'condition_id' => 'Condition ID',
            'rule_id' => 'Rule ID',
            'weight' => 'Weight',
            'is_active' => 'Is Active',
        ];
    }

    public function getCondition()
    {
        return $this->hasOne(BusinessConditions::class, ['id' => 'condition_id']);
    }

    public function getRule()
    {
        return $this->hasOne(BusinessRules::class, ['id' => 'rule_id']);
    }
}
