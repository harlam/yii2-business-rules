<?php

namespace harlam\yii2\BusinessRules\models;

use harlam\BusinessRules\BusinessRule;
use harlam\BusinessRules\interfaces\BusinessRuleInterface;

/**
 * This is the model class for table "business_rules".
 *
 * @property int $id
 * @property string $group
 * @property string $name
 * @property string $description
 * @property string $rule
 */
class BusinessRules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'name', 'rule'], 'required'],
            [['description', 'rule'], 'string'],
            [['group', 'name'], 'string', 'max' => 255],
            [['group', 'name'], 'unique', 'targetAttribute' => ['group', 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group' => 'Group',
            'name' => 'Name',
            'description' => 'Description',
            'rule' => 'Rule',
        ];
    }

    public function getBusinessRule(): BusinessRuleInterface
    {
        return new BusinessRule($this->name, $this->rule);
    }
}
