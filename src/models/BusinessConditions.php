<?php

namespace harlam\yii2\BusinessRules\models;

use harlam\BusinessRules\BusinessCondition;
use harlam\BusinessRules\interfaces\BusinessConditionInterface;

/**
 * This is the model class for table "business_conditions".
 *
 * @property int $id
 * @property string $group
 * @property string $name
 * @property string $description
 * @property string $condition
 */
class BusinessConditions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_conditions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'name', 'condition'], 'required'],
            [['description', 'condition'], 'string'],
            [['group', 'name'], 'string', 'max' => 255],
            [['group', 'name'], 'unique', 'targetAttribute' => ['group', 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group' => 'Group',
            'name' => 'Name',
            'description' => 'Description',
            'condition' => 'Condition',
        ];
    }

    public function getBusinessCondition(): BusinessConditionInterface
    {
        return new BusinessCondition($this->name, $this->condition);
    }
}
