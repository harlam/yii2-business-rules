<?php

use yii\db\Migration;

class m180831_081551_business_rules_set extends Migration
{
    /**
     * Типы бизнес-объектов
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('business_rules_set', [
            'id' => $this->primaryKey(),
            'group' => $this->string()->notNull(),
            'condition_id' => $this->integer()->notNull(),
            'rule_id' => $this->integer()->notNull(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'is_active' => $this->boolean()->notNull()->defaultValue(true),
        ]);

        $this->createIndex('idx_business_rules_set_condition_id_and_rule_id_unq', 'business_rules_set', ['condition_id', 'rule_id'], true);
        $this->createIndex('idx_business_rules_set_weight', 'business_rules_set', 'weight');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('business_rules_set');
    }
}
