<?php

use yii\db\Migration;

class m180831_081221_business_rules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('business_rules', [
            'id' => $this->primaryKey(),
            'group' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'rule' => $this->text()->notNull(),
        ]);

        $this->createIndex('idx_business_rules_group_and_name_unq', 'business_rules', ['group', 'name'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('business_rules');
    }
}
