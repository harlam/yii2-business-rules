<?php

use yii\db\Migration;

class m180831_081155_business_conditions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('business_conditions', [
            'id' => $this->primaryKey(),
            'group' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'condition' => $this->text()->notNull(),
        ]);

        $this->createIndex('idx_business_conditions_group_and_name_unq', 'business_conditions', ['group', 'name'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('business_conditions');
    }
}
