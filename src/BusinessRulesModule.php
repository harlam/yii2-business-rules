<?php

namespace harlam\yii2\BusinessRules;

use yii\base\Module;

class BusinessRulesModule extends Module
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'harlam\yii2\BusinessRules\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
